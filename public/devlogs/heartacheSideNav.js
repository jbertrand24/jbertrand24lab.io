// side navigation template for devlogs
class heartacheSideNav extends HTMLElement {
    constructor() {
        super();
        this.innerHTML = 
        `
          <div class="card"style="position: -webkit-sticky; /* Safari */ position: sticky; top: 1rem; border-width: 0px 2px 2px 0px">
          <center>
              <p> Project Name Index</p>
          </center>
          <div class="card-body">
              <ul class="list-group">
                  <li class="list-group-item"><a href="introduction.html">Introduction</a></li>
                  <li class="list-group-item"><a href="log1.html">Devlog #1 - Basic movement (ground movement)</a></li>
                  <li class="list-group-item"><a href="log2.html">Devlog #2 - Basic movement (air movement)</a></li>
              </ul>
          </div>
        `
    }
    
}

window.customElements.define('heartache-side-nav', heartacheSideNav);