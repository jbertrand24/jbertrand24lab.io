// side navigation template for devlogs
class DevlogTopNav extends HTMLElement {
    constructor() {
        super();
        this.innerHTML = 
        `
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="/devlogs/index.html">MinouPitou Devlogs</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/devlogs/index.html">All Devlogs</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        `
    }
    
}

window.customElements.define('devlog-top-nav', DevlogTopNav);